
docker build --rm --no-cache --network host --tag=http-router .;
docker container stop http-router;
docker container rm http-router;
docker create \
  --name http-router \
  --publish 80:80 \
  --publish 443:443 \
  --volume 'c:\Users\luka8088\box\instance\http-router':'/data' \
  http-router \
;

docker container start http-router;

winpty docker exec -it http-router http-router reload
