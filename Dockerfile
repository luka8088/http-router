
FROM alpine:3.10

VOLUME /data

COPY . /code
COPY docker/. /

RUN chmod +x /setup.sh && /setup.sh

EXPOSE 80
EXPOSE 443

STOPSIGNAL SIGTERM

ENTRYPOINT /start.sh
