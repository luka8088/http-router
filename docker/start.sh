#!/bin/sh

set -eu;
set -o errexit;
set -o pipefail;

export dataPath=/data;
export tempPath=/tmp;

mkdir -p /data/acme/;

if [ ! -f /data/acme/dhparam.pem ]; then
  openssl dhparam -dsaparam -out /data/acme/dhparam.pem 4096
fi;

http-router;

nginx -t;
nginx -g 'daemon off;';

#while [ 1 ]; do sleep 30; done;

# export Namecom_Username=luka8088
# export Namecom_Token=x
# acme.sh --home /data/acme --issue -d luka8088.devpc.donatr.io --dns dns_namecom

# crossplane parse /data/sites/luka8088.devpc.donatr.io
