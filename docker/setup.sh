#!/bin/sh

set -eu;
set -o errexit;
set -o pipefail;

apk add --no-cache sudo;
apk add --no-cache nginx;
apk add --no-cache libressl acme.sh ca-certificates;
apk add --no-cache socat;
apk add --no-cache php7 php7-json;
apk add --no-cache py2-pip;

pip install crossplane;
#pip install yq;

#echo 'http://dl-cdn.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories;
#echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories;
#apk add --no-cache gcc libc-dev llvm9-libs ldc ldc-runtime ldc-static dtools-rdmd;

# https://github.com/gliderlabs/docker-alpine/issues/185
# nginx: [emerg] open() "/run/nginx/nginx.pid" failed (2: No such file or directory)
mkdir -p /run/nginx;
chown nginx:nginx /run/nginx;

chmod +x /start.sh;
chmod +x /usr/bin/http-router;
chmod +x /usr/bin/reload;
